var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sslRedirect = require('heroku-ssl-redirect');

global.passport = require('passport');
var index = require('./routes/index');
var users = require('./routes/users');
var coins = require('./routes/coins');
var icos = require('./routes/icos');
var email = require('./routes/email');
var blog = require('./routes/blog');
var portfolio = require('./routes/list');
var analytics = require('./routes/analytics');
var graphs = require('./routes/graphs');


//DB Setup
var monk = require('monk');
var user = process.env.DB_USER || 'admin';
var pass = process.env.DB_PASS || 'icocitydbadmin';
var uri = process.env.DB_URI || '@ds155362-a0.mlab.com:55362,ds155362-a1.mlab.com:55362/heroku_7gbs4db5?replicaSet=rs-ds155362';
var url = user + ':' + pass + uri;
var secret = process.env.AUTH_SECRET || 'devsecret123';

// var monk = require('monk');
// var user = process.env.DB_USER;
// var pass = process.env.DB_PASS;
// var uri = process.env.DB_URI;
// var url = user + ':' + pass + uri;
// var secret = process.env.AUTH_SECRET || 'devsecret123';

//auth
var session = require('express-session');
const MongoStore = require('connect-mongo')(session);
var morgan = require('morgan');
var flash = require('connect-flash');

if (user == undefined || pass == undefined) {
  url = 'localhost'
}

global.db = monk(url);
var mongoose = require('mongoose');
mongoose.connect(url);

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views/admin_7_rounded'));
app.set('view engine', 'hbs');


//cache
const NodeCache = require( "node-cache" );
global.cache = new NodeCache({ stdTTL: 59, checkperiod: 59 });

// enable ssl redirect
app.use(sslRedirect());

// Remote API fetch setup
var request = require('request');
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

require('./config/passport')(global.passport);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

//app.use(session({ secret: secret })); // session secret
app.use(
  session({
    store: new MongoStore({
    mongooseConnection: mongoose.connection
  }),
  cookie: { maxAge: 120000000 },
  secret: 'yieiuhqwdasi8888k'
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session*/


app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));

// script sourcepath
app.use('/scripts', express.static(__dirname + '/vendor/'));
app.use('/', index);
app.use('/users', users);
app.use('/coins', coins);
app.use('/icos', icos);
app.use('/email', email);
app.use('/blog', blog);
app.use('/list', portfolio);
app.use('/analytics', analytics);
app.use('/graphs', graphs);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000);
app.db = db;
module.exports = app;