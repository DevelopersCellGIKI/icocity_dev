const axios= require('axios');
const mongoose= require('mongoose');
var Schema = mongoose.Schema;

//let mongoUrl = "mongodb://admin:admin@ds233228.mlab.com:33228/ico_city";
//process.argv[] is used for getting data from command line
//ex: node index.js a b c d 
//process.argv[0]=node, process.argv[1]=index.js and so on
module.exports.start= function(){
    var coin=process.argv[2] || 'bitcoin', //the coin for which data is to be fetched
     startDate=process.argv[3] || '1367174841000', endDate=process.argv[4] || '1518523142000', //start and end dates
     mongoUser=process.argv[5] || 'admin', 
     mongoPass=process.argv[6] || 'admin', 
     mongoUri=process.argv[7] || '@ds233228.mlab.com:33228/ico_city';
   console.log(coin + '  ' + startDate + '  '+ endDate);
    var mongoUrl = "mongodb://"+mongoUser+":"+mongoPass + mongoUri;
     mongoose.connect(mongoUrl,(err)=>{
        if(err) console.log(err);
        console.log("connected");
        this.dumpInMongo(coin,startDate,endDate);

     });
}
//a mongodb schema created
var historicalCoinData = new mongoose.Schema({
  coin: String,
  market_cap_by_available_supply: Array,
  price_btc: Array,
  price_usd: Array,
  volume_usd:Array
});
var coinData = mongoose.model('coinData', historicalCoinData);
module.exports.dumpInMongo= function(coin, startDate, endDate) {
    
    if(!isDate(startDate) && !isDate(endDate)){ //checks that whether date is in unix timestamp or not
        getCoinData(coin,startDate,endDate).then((response,err) => {
        if(err){
             console.log(err);
        }
            //find out that if a document of a coin is already present or not
           coinData.findOne({ 'coin' : coin },function (err, coinDetail) {
            if (err) return handleError(err);
            //if present then update the existing document
            if(coinDetail){
                coinDetail.set({market_cap_by_available_supply : response.market_cap_by_available_supply,
                    price_btc : response.price_btc,
                    price_usd : response.price_usd,
                    volume_usd : response.volume_usd
                });
                coinDetail.save(function (err, updatedCoin) {
                    if (err) return handleError(err);
                    console.log("coin has been updated");
                });
            }
            //else create a new document
            else{

                coinData.create({coin : coin,
                    market_cap_by_available_supply: response.market_cap_by_available_supply,
                    price_btc: response.price_btc,
                    price_usd : response.price_usd,
                    volume_usd : response.volume_usd
                 }, function (err, small) {
                        if (err) return handleError(err);
                            console.log("New coin inserted into db");
                        })
            }
            
        })
    })
    }
    else{ //if date not in unix timestamp
        console.log("date should be in unix time stamp");
    }
}
//get coin details from the api
var getCoinData = (coin,startDate,endDate) => {

    var url = "https://graphs2.coinmarketcap.com/currencies/" + coin + "/" + startDate + "/" + endDate + "/";
    console.log(url); 
    return new Promise((resolve, reject) => {
        axios.get(url)
          .then(function (response) {
              if(response.status == '200'){

                resolve(response.data);
              }
              else{
                  reject('there was some problem with the api')
              }
            //console.log(response);
            })
           .catch(function (error) {
               reject(error);
                console.log(error);
           });
    })
}

function isDate (date) {
    return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}
this.start();
