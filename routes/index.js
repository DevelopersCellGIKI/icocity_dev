var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

/* GET home page. */
///<a value="My Portfolio" data-toggle="modal" href="#basic">My Tokens</a>
     //                         <a href="/list">My Tokens</a>
router.get('/', function(req, res, next) {
   
    if (req.user){

        res.render('index', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
       
    }
     res.render('index', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
   
  	
});

router.get('/advanced', function(req, res, next) {
  	var eth_price = cache.get( "eth_price");
  	var btc_price = cache.get( "btc_price");

  	if (req.user){
        if (eth_price == undefined || btc_price == undefined) {
            console.log('CACHE MISS PRICE');
            coll = db.get('coins');
            coll.find({ symbol: { $in: [ "BTC", "ETH" ] } }, { "_id" : 0, "symbol" : 1, "price_usd" : 1 }).then((docs) => {
                var b_price = docs.find( function(e) { return e.symbol == "BTC" } );
                var e_price = docs.find( function(e) { return e.symbol == "ETH" } );
                cache.set("eth_price", e_price.price_usd);
                cache.set("btc_price", b_price.price_usd);
                res.render('advanced', { title: "Token Data I News, data and analytics for all ICO’s and tokens", "btc_price" : b_price.price_usd, "eth_price": e_price.price_usd, portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>' });	
            });
            } else {
                res.render('advanced', { title: "Token Data | News, data and analytics for all ICO’s and tokens", "btc_price" : btc_price, "eth_price": eth_price , portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});	
            }
     } else{
        if (eth_price == undefined || btc_price == undefined) {
            console.log('CACHE MISS PRICE');
            coll = db.get('coins');
            coll.find({ symbol: { $in: [ "BTC", "ETH" ] } }, { "_id" : 0, "symbol" : 1, "price_usd" : 1 }).then((docs) => {
                var b_price = docs.find( function(e) { return e.symbol == "BTC" } );
                var e_price = docs.find( function(e) { return e.symbol == "ETH" } );
                cache.set("eth_price", e_price.price_usd);
                cache.set("btc_price", b_price.price_usd);
                res.render('advanced', { title: "Token Data I News, data and analytics for all ICO’s and tokens", "btc_price" : b_price.price_usd, "eth_price": e_price.price_usd, portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>' });	
            });
            } else {
                res.render('advanced', { title: "Token Data | News, data and analytics for all ICO’s and tokens", "btc_price" : btc_price, "eth_price": eth_price , portfolio:'<input value="My Tokens" type="button" class="btn" style ="background:#0071bc;color:white;margin-left:10px;float:right" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});	
            }
  
         
     }
});

router.get('/upcoming', function(req, res, next) {
//    up = db.get('upcoming');
//    act = db.get('active');
//    up.find().then((ups) => {
//        var upString = JSON.stringify(ups);
//
//        act.find().then((acts) => {
//            var actString = JSON.stringify(acts);
//            if (req.user) {
//                res.render('upcoming', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn"  id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>', upcoming: upString, active: actString});
//            } else {
//                res.render('upcoming', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a id = "navListLink" value="My Portfolio" data-toggle="modal" href="#basic">My Tokens</a>', upcoming: upString, active: actString});    
//            }
//        });
//    });
    
     if (req.user){

        res.render('upcoming', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
       
    }
     res.render('upcoming', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
});

router.get('/gambling', function(req, res, next) {
    if (req.user){

        res.render('gambling', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
       
    }
     res.render('gambling', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
});

router.get('/active', function(req, res, next) {

    
     if (req.user){

        res.render('active', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
       
    }
     res.render('active', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
});


router.get('/neo', function(req, res, next) {

    
     if (req.user){

        res.render('neo', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
       
    }
     res.render('neo', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
});










// process the login form
router.post('/login', passport.authenticate('local-login', {
    successRedirect : '/list', // redirect to the secure profile section
    failureRedirect : '/', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

router.get('/login', function(req, res, next) {
    res.render('login', { message: req.flash('loginMessage') });
});


// process the signup form
router.post('/signup', global.passport.authenticate('local-signup', {
    successRedirect : '/list', // redirect to the secure profile section
    failureRedirect : '/', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
}));

/*router.get('/signup', function(req, res, next) {
    res.render('signup', { message: req.flash('signupMessage') });
});*/

router.get('/profile', isLoggedIn, function(req, res) {
    res.render('list', {
        user : req.user // get the user out of session and pass to template
    });
});

router.get('/logout', function(req, res) {
   console.log('here');
    req.logout();
    res.send('done');
    /*res.render('index', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">'});*/
});


// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

//Auth + routes for portfolio
module.exports = router;
