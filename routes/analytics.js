var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

/* GET analytics. */
router.get('/bitcointalksearch', function(req, res, next) {
  res.render('bitcointalksearch', {});
});

router.get('/', function(req, res, next) {
  if (req.user) {
    res.render('analytics', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/list" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>', navList: '<a href="/list">My Tokens</a>'});
  }
  res.render('analytics', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">', navList:'<a value="My Portfolio" data-toggle="modal" href="#basic" id = "navListLink">My Tokens</a>'});
});


module.exports = router;
