var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

/* GET icos. */
router.get('/', function(req, res, next) {
  	var icos = cache.get( "icos" );
  	if (icos == undefined) {
  		console.log("CACHE MISS");
  		var coll = db.get('icos');
	  	coll.find().then((docs) => {
	  		cache.set('icos', docs);
		  	res.json({'data': docs});
	  	});
  	} else {
  		res.json({'data':icos});
  	}
});

router.get('/advanced', function(req, res, next) {
  	var icos = cache.get( "icos_advanced" );
  	if (icos == undefined) {
  		console.log("CACHE MISS");
  		var coll = db.get('icos_advanced');
	  	coll.find().then((docs) => {
	  		cache.set('icos_advanced', docs);
		  	res.json({'data': docs});
	  	});
  	} else {
  		console.log(icos);
  		res.json({'data':icos});
  	}
});

router.get('/upcoming', function(req, res, next) {
  	var coll = db.get('upcoming');
  	coll.find().then((docs) => {
	  	res.json({'data':docs});
          res.status(200);
  	});
});

router.get('/active', function(req, res, next) {
  	var coll = db.get('active');
  	coll.find().then((docs) => {
	  	res.json({'data':docs});
          res.status(200);
  	});
});

router.get('/neo', function(req, res, next) {
    var coll = db.get('neo');
    coll.find().then((docs) => {
      res.json({'data':docs});
          res.status(200);
    });
});

router.post('/portfolio', function(req ,res ,next) {
  var coll = db.get('icos');
  // empty dictionary sent as req.body if user has no tokens
  if (req.body.tokens) {
    coll.find({name: {$in: req.body.tokens} }).then((docs) => {
      res.json({'data':docs});
      res.status(200);
    });
  }
  else {
    res.json({'data':[]});
    res.status(200);
  }
})

module.exports = router;
