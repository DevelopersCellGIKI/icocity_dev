var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');

/* GET icos. */
router.get('/', function(req, res, next) {

    if (req.user){

        res.render('list', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<a href = "/" style ="padding:0px;margin:0px"> <input value="My Tokens" type="button" class="btn" id="directPort"> </a>'});
       
    } else{
        res.render('index', { title: "Token Data | News, data and analytics for all ICO’s and tokens", portfolio:'<input value="My Tokens" type="button" class="btn" id="portfolio" data-toggle="modal" href="#basic">'});
    }
     
});
router.get('/ticker', function(req, res, next) {
  	var icos = cache.get( "icos" );

  	if (icos == undefined) {
  		console.log("CACHE MISS");
  		var coll = db.get('icos');
	  	coll.find().then((docs) => {
	  		cache.set('icos', docs);
		  	res.json({'data': docs});
	  	});
  	} else {
  		res.json({'data':icos});
  	}
});



module.exports = router;
