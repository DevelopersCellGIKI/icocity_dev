var express = require('express');
var router = express.Router();
var json_utils = require('../util/json_utils');
//retrieving stored info about coins(graph 2)
var coinDailyData = require('./data/data2');

var timestamp = require('unix-timestamp');
//used in sorting
var months = {
    Jan: "January",
    Feb: "February",
    Mar: "March",
    Apr: "April",
    May: "May",
    Jun: "June",
    Jul: "July",
    Aug: "August",
    Sep: "September",
    Oct: "October",
    Nov: "November",
    Dec: "December"
}



router.get('/multiline', function(req, res, next) {
    var historicalData = JSON.parse(JSON.stringify(coinDailyData));

    var data = {
        labels: [],
        datasets: [{
            label: 'USD Raised in Millions',
            data: [],
            backgroundColor: "rgba(61, 6, 116,.99)",
            borderColor: "rgba(54, 162, 235, 1)",
            borderWidth: 1
        }]
    };
    // List of Available coins.
    getAvailableCoins("BTC", historicalData).then(response => {
        var datacoins = response;
        var coinDropDown = getCoinList(datacoins);

        if (req.query.selectpicker === undefined) {
            res.render('graphs_multiline', {
                "title": "graphs",
                "coinDropDown": json_utils.encodeJSON(coinDropDown)
            });

        } else {

            datacoins = [];
            for (var i = 0; i < req.query.selectpicker.length; i++) {
                var id = req.query.selectpicker[i];
                datacoins.push(coinDropDown.data[id].name);
            }



            getMultiLineData("BTC", datacoins, historicalData).then(response2 => {
                var values = response2;
                var newestDateAvailable = "2001-04-05";

                for (var coin in values) {
                    values[coin].sort(sortWithDate); // sorts date in decreasing order in unixTimeStamp format 

                    if (typeof(values[coin][0].date) !== "string") {
                        values[coin] = convertCoinArrayDate(values[coin]); //converts unixTimeStamp dates to YYYY-MM-DD if already not converted
                    }
                    a = new Date(values[coin][0].date);
                    b = new Date(newestDateAvailable);
                    if (a > b) {
                        newestDateAvailable = values[coin][0].date;
                    }



                }

                var b = new Date(newestDateAvailable);
                var graphValues = {};
                for (var coin in values) {
                    var i = -1;
                    while (new Date(values[coin][++i].date) < b);
                    graphValues[coin] = []
                    for (var j = i; j < (values[coin].length); j++) {
                        graphValues[coin].push(values[coin][j]);
                    }

                }

                var labels = [];
                for (var i = 0; i < graphValues[datacoins[0]].length; i++) {
                    labels.push(graphValues[datacoins[0]][i].date);
                }

                var tempDataset = []
                for (var coin in graphValues) {
                    tempDataset.push({});
                    tempDataset[tempDataset.length - 1].label = coin;
                    tempDataset[tempDataset.length - 1].fill = false;
                    tempDataset[tempDataset.length - 1].backgroundColor = "rgba(" + getRandomColor() + "," + getRandomColor() + "," + getRandomColor() + "," + ".37)";
                    tempDataset[tempDataset.length - 1].borderColor = "rgba(" + getRandomColor() + "," + getRandomColor() + "," + getRandomColor() + "," + "1)";
                    tempDataset[tempDataset.length - 1].pointRadius = 0;
                    tempDataset[tempDataset.length - 1].borderWidth = 1;

                    tempDataset[tempDataset.length - 1].data = [];
                    for (var i = 0; i < graphValues[coin].length; i++) {
                        tempDataset[tempDataset.length - 1].data.push(graphValues[coin][i].close);
                    }


                }


                data.labels = labels;
                data.datasets = tempDataset;
                var acData = data;

                res.render('graphs_multiline', {
                    "title": "graphs",
                    "data": json_utils.encodeJSON(acData),
                    "coinDropDown": json_utils.encodeJSON(coinDropDown)
                });

            });
        }



    });

});





router.get('/coinHistoricalData', function(req, res, next) {
    res.json(coinDailyData);
})

router.get('/line', function(req, res, next) {
    var data = {
        labels: [],
        datasets: [{
            label: 'USD Raised in Millions',
            data: [],
            backgroundColor: "rgba(61, 6, 116,.99)",
            borderColor: "rgba(54, 162, 235, 1)",
            borderWidth: 1
        }]
    };
    getIcoData().then((response) => {
        var values = response;
        values.sort(sorter); //sorts Date in decreasing order
        var Icolabels = [];
        var Icodata = [];
        for (var i = 0; i < values.length; i++) {
            if (values[i]._id === '' || values[i]._id === ' ' || values[i]._id === null) continue;
            if (values[i].total <= 0) continue; // remove icos with value <=0
            Icolabels.push(values[i]._id);
            Icodata.push(values[i].total);
        }
        Icolabels.reverse();
        Icodata.reverse();
        data.labels = Icolabels;

        data.datasets[0].data = Icodata;

        res.render('graphs_line', {
            "title": "graphs",
            "data": json_utils.encodeJSON(data)
        });

    });


});

router.get('/', function(req, res, next) {
    var data = {
        labels: [],
        datasets: [{
            label: 'USD Raised in Millions',
            data: [],
            backgroundColor: "rgba(61, 6, 116,.99)",
            borderColor: "rgba(54, 162, 235, 1)",
            borderWidth: 1
        }]
    };
    getIcoData().then((response) => {
        var values = response;
        values.sort(sorter);
        var Icolabels = [];
        var Icodata = [];
        for (var i = 0; i < 10; i++) {
            if (values[i]._id === '' || values[i]._id === ' ' || values[i]._id === null) continue;
            if (values[i].total <= 0) continue;
            Icolabels.push(values[i]._id);
            Icodata.push(values[i].total);
        }
        Icolabels.reverse();
        Icodata.reverse();
        data.labels = Icolabels;

        data.datasets[0].data = Icodata;

        res.render('graphs', {
            "title": "graphs",
            "data": json_utils.encodeJSON(data)
        });

    });


});

var getIcoData = function() {

    var coll = db.get('icos');
    return coll.aggregate([{
                $match: {
                    status: "Completed"
                }
            },
            {
                $group: {
                    _id: "$month",
                    total: {
                        $sum: "$usd_raised"
                    }
                }
            }
        ])

        .then((docs) => {
            return docs;
        });

}

var getCoinList = function(selected) {
    var coinDropDown = {
        data: [],
        a: "aaaa"

    };
    for (var i = 0; i < selected.length; i++) {
        coinDropDown.data.push({
            name: selected[i],
            id: i
        })
    }
    return coinDropDown;
}


var getAvailableCoins = function(baseCoin, historicalData) {
    if (typeof(baseCoin) === 'undefined') baseCoin = "ETH";
    var names = [];

    return new Promise((resolve, reject) => {
        historicalData[baseCoin].forEach(i => {
            names.push(i.name);
        });

        resolve(names);

    }).then((res) => {
        return res;
    });


}

// getMultiLineData(baseCoin, selected, historicalData) should fetch all the coins listed in 'selected'and the price should be in 'baseCoin'
// historicalData can be replaced by DB object
// in the current scenario, it refers to the file containing the historical data
var getMultiLineData = function(baseCoin, selected, historicalData) {
    if (typeof(baseCoin) === 'undefined') baseCoin = "ETH";
    if (typeof(selected) === 'undefined') selected = ["ETC"];


    var data = {};

    // this should be replaced by an API/DB call to get daily data for the required coins
    return new Promise((resolve, reject) => {
        historicalData[baseCoin].forEach(i => {
            if (selected.indexOf(i.name) !== -1) {
                data[i.name] = i.data;
            }

        });
        resolve(data);

    }).then((res) => {
        return res;
    });

}

var sorter = function(a, b) {
    d = a._id.split(" ");
    c = b._id.split(" ");
    return new Date(months[c[0]] + c[1]) - new Date(months[d[0]] + d[1])
};

var sortWithDate = function(a, b) {
    //assumed date format is in unix timestamp format
    c = a.date;
    d = b.date;
    return c - d;
}

//return random number for rgb color value
function getRandomColor() {
    return ((Math.floor(Math.random() * (150 + 1))));
}

var unixTimestampTodate = function(unixTimestamp) {
    return timestamp.toDate(unixTimestamp).toISOString().slice(0, 10);
}

//converts unixTimeStamp dates to YYYY-MM-DD
var convertCoinArrayDate = function(coin) {
    coin.forEach(t => {
        t.date = unixTimestampTodate(t.date);

    });
    return coin;
}



module.exports = router;