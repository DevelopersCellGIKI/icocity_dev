var express = require('express');
var router = express.Router();
var User = require('../models/user');
/* GET users listing. */
router.post('/add', function(req, res, next) {

	var ticker = req.body.ticker;
	var email = req.user.local.email;
  	if (email == undefined || email == "" || ticker == undefined) {
  		console.log("Ticker must be in request body");
  		res.sendStatus(400);
  		return;
  	}

	User.findOne({ 'local.email' :  email }, function (err, user) {
	  if (err || user == undefined) {
	  	console.log(err);
	  	res.sendStatus(400);
	  	return;
	  }

	  var set = new Set(user.tokens);
	  set.add(ticker);
	  user.tokens = Array.from(set);

	  user.save(function (err, updatedTank) {
	    if (err) {
	    	res.sendStatus(500);
	    	return;
	    }
	    res.sendStatus(200);
	    return;
	  });
	});
});

router.post('/delete', function(req, res, next) {

	var ticker = req.body.ticker;
	var email = req.user.local.email;
  	if (email == undefined || email == "" || ticker == undefined) {
  		console.log("Ticker must be in request body");
  		res.sendStatus(400);
  		return;
  	}

	User.findOne({ 'local.email' :  email }, function (err, user) {
	  if (err || user == undefined) {
	  	console.log(err);
	  	res.sendStatus(400);
	  	return;
	  }

	  var set = new Set(user.tokens);
	  set.delete(ticker);
	  user.tokens = Array.from(set);

	  user.save(function (err, updatedTank) {
	    if (err) {
	    	res.sendStatus(500);
	    	return;
	    }
	    res.sendStatus(200);
	    return;
	  });
	});
});

router.get('/data', function(req, res, next) {
	res.json(req.user);
});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
}

module.exports = router;
