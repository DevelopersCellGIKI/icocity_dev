### crypto-thing
------------

#### Pre-reqs
-----------
* install npm
* run `npm install` in the root directory


#### How to run locally
-----------
* run `npm start`


#### How to deploy to Firebase
----------
* run `npm build` which compiles the site to a static bundle located in `/build` directory
* run `firebase deploy` which takes the contents of `/build` directory and hosts it at [crypto-thing.firebaseapp.com](crypto-thing.firebaseapp.com)


#### Firebase stuff
----------
* view data [here](https://console.firebase.google.com/u/0/project/crypto-thing/database/data/)
* look at deploy history [here](https://console.firebase.google.com/u/0/project/crypto-thing/hosting/main)
