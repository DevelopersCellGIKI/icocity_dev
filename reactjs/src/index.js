import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './components/Dashboard'
import registerServiceWorker from './registerServiceWorker';

// import Routes from './routes';
import {BrowserRouter as Router, Route, Switch, IndexRoute, Link} from 'react-router-dom';

const Routes = () => (
  <Switch>
    <Route exact path ="/analytics" component = {Dashboard} />
  </Switch>
)

// ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
  <Router>
    <Routes />
  </Router>,
 document.getElementById('root')
);
registerServiceWorker();
