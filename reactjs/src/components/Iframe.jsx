import React, {Component} from 'react';
import * as firebase from 'firebase';
import {BrowserRouter as Router, Route, Switch, IndexRoute, Link} from 'react-router-dom';
import ReactDOM, {findDOMNode} from 'react-dom';
import $ from 'jquery';
import './style/IFrame.css'

class Iframe extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return(         
      <div className='IFrameContainer'> 
        <iframe src={this.props.src}/>
      </div>
    )
  }
}

export default Iframe;