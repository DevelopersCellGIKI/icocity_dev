import React, {Component} from 'react';
import $ from 'jquery';

class BitcoinTalkSearch extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const embedcode = 
      `<script>
        (function() {
          var cx = '016660200577587308545%3Aesf40ml9aag';
          var gcse = document.createElement('script');
          gcse.type = 'text/javascript';
          gcse.async = true;
          gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
          var s = document.getElementsByTagName('script')[0];
          s.parentNode.insertBefore(gcse, s);
        })();
      </script>
      <gcse:search linkTarget='_blank'></gcse:search>`

    $('.SearchContainer').html(embedcode)
  }

  render() {
    return(         
      <div className={`SearchContainer Scrollable`}> 
      </div>
    )
  }
}

export default BitcoinTalkSearch;