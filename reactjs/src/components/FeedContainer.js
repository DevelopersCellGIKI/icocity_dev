import React, {Component} from 'react';
import './style/FeedContainer.css';

class FeedContainer extends Component {

  constructor(props) {
    super(props)

    this.state = { width: '0'};
    this.updateWindowWidth = this.updateWindowWidth.bind(this);
  }

  componentDidMount() {
    this.updateWindowWidth();
    window.addEventListener('resize', this.updateWindowWidth);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowWidth);
  }

  updateWindowWidth() {
    this.setState({ width: window.innerWidth - 37 });
  }

  render() {
    let { width } = this.state

    var pixelWidth = (width - (this.props.feedCount*2 + 2) * 5) / this.props.feedCount
    pixelWidth = Math.max(pixelWidth, 225)

  	return (
  		<div className='FeedContainer' style={{width: pixelWidth}}>
	  		<div className='FeedHeader'>
	  			<img src={ this.props.icon }/>
	  			<h2>{ this.props.header }</h2>
	  		</div>
	  		<div className='FeedContent'>
	  			{ this.props.children }
	  		</div>
        </div>
  	);
  }
}

export default FeedContainer;