import React, {Component} from 'react';
import * as firebase from 'firebase';
import {BrowserRouter as Router, Route, Switch, IndexRoute, Link} from 'react-router-dom';

class Summary extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: this.props.name,
      websiteURL: this.props.websiteURL,
      whitepaperURL: this.props.whitepaperURL
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props != nextProps) {
      this.props = nextProps

      console.log(this.props)
    }
  }

  render() {
    let { name, websiteURL, whitepaperURL } = this.state

    return (
      <div style={{marginBottom: 10}}>
        <Link to={`/${name}`}>{name}</Link>
        <div> { websiteURL } </div>
      </div>
    )
  }
}

export default Summary;