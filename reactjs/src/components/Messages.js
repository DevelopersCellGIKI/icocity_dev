import React, {Component} from 'react';
import ReactFire from 'reactfire';
import firebase from '../Firebase';
import moment from 'moment'
import base from './../rebase';
import Linkify from 'react-linkify'
import PinIcon from './images/pin_icon.png'
import './style/Messages.css';
import Request from 'axios-request-handler';

class Messages extends Component {
  componentWillMount() {
    this.loadFeed()
  }

  componentWillReceiveProps(nextProps) {
    this.props = nextProps
    this.loadFeed()
  }

  componentWillUnmount() {
    this.requestInstance.cancel();
  }

  loadFeed() {
    this.setState({loading: true})
    this.requestInstance = new Request('https://cryptohelper.io/api/messages/' + this.props.icoName + '/' + this.props.feedType)
    this.requestInstance.poll(60 * 1000).get((res) => {
      this.setState({feed: res.data, loading: false, showPinned: false})
    });
  }

  selectedChannel(event) {
    this.setState({selectedChannel: event.target.value, showPinned: false})
  }

  showPinned(shouldShowPinned) {
    this.setState({showPinned: shouldShowPinned})
  }

  render() {
    if (this.state.loading) {
      return (
        <div className='MessagesContainer'>
        </div>
      );
    }

    let feed = this.state.feed

    var ChannelSwitchContainer;

    if (this.props.feedType == 'telegram_feed') { // Telegram doesn't have channels or pinned messages
      let messages = feed['messages']
      ChannelSwitchContainer = // Should move this into its own component later
          <div className='ChannelSwitchContainer'>
            <div>
              <h4>Total users: {feed['analytics']['group_size']}</h4>
            </div>
          </div>
      return (
        <div className='MessagesContainer'>
          {ChannelSwitchContainer}
          <Linkify properties={{target: '_blank'}}>
          <div className='Scrollable'>
            {messages.map(function(message, index) {
                return (
                  <div className='MessageContainer' key={index}>
                    <p><b>{message.author}</b> <span className='MessageTimestamp'>{moment.unix(message.timestamp).format("MM-DD-YYYY HH:mm:ss")}</span></p>
                    <p>{message.text}</p>
                  </div>
                );
              })}
          </div>
          </Linkify>
        </div>
      );
    } else {
      let channels = feed['channels']
      var selectedChannel = this.state.selectedChannel

      if (!selectedChannel) {
        selectedChannel = Object.keys(this.state.feed.channels)[0]
      }

      var messages;
      var pinned = <div></div>

      if ('pinned_messages' in feed['channels'][selectedChannel]) {
        pinned = <img onClick={() => this.showPinned(true)} className='PinIcon' src={PinIcon} />
      }

      var options = []

      Object.keys(channels).map((channel, index) => {
        if (selectedChannel == channel) {
          options.push(<option selected value={channel}>{channel}</option>)
        } else {
          options.push(<option value={channel}>{channel}</option>)
        }
      })

      console.log(feed)

      if (!this.state.showPinned) {
        messages = feed['channels'][selectedChannel]['messages']
        ChannelSwitchContainer = // Should move this into its own component later
          <div className='ChannelSwitchContainer'>
            <div>
              <h4>Total users: {feed['analytics']['group_size']}</h4>
            </div>
            <h4>Channel: </h4>
            <select onChange={this.selectedChannel.bind(this)}>
              {options}
            </select>
            {pinned}
          </div>
      } else {
        messages = feed['channels'][selectedChannel]['pinned_messages']
        ChannelSwitchContainer =
          <div className='ChannelSwitchContainer'>
            <h4 className='PinTitle'>Pinned messages in {selectedChannel}</h4>
            <a className='PinCloseButton' onClick={() => this.showPinned(false)}>Back</a>
          </div>
      }

      return (
        <div className='MessagesContainer'>
          {ChannelSwitchContainer}
          <Linkify properties={{target: '_blank'}}>
          <div className={`Messages Scrollable`}>
            {messages.map(function(message, index) {
                return (
                  <div className='MessageContainer' key={index}>
                    <p><b>{message.author}</b> <span className='MessageTimestamp'>{moment.unix(message.timestamp).format("MM-DD-YYYY HH:mm:ss")}</span></p>
                    <p>{message.text}</p>
                  </div>
                );
            })}
          </div>
          </Linkify>
        </div>
      );
    }
  }
}

export default Messages;