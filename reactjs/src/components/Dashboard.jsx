import React, {Component} from 'react';
import Select from 'react-select';
import Messages from './Messages';
import Embed from './Embed.jsx'
import Iframe from './Iframe.jsx'
import FeedContainer from './FeedContainer'
import Checkbox from './Checkbox'
import './style/Dashboard.css'
import 'react-select/dist/react-select.css';
import TwitterLogo from './images/twitter_logo.png'
import RedditLogo from './images/reddit_logo.png'
import TelegramLogo from './images/telegram_logo.png'
import DiscordLogo from './images/discord_logo.png'
import BitcoinTalkLogo from './images/bitcointalk_logo.png'
import SlackLogo from './images/slack_logo.png'
import RocketChatLogo from './images/rocket_chat_logo.png'
import Request from 'axios-request-handler';

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      icoData: {},
      selectedCheckboxes: new Set()
    }
  }

  toggleCheckbox(label) {
    if (this.state.selectedCheckboxes.has(label)) {
      this.state.selectedCheckboxes.delete(label);
    } else {
      this.state.selectedCheckboxes.add(label);
    }

    this.setState({selectedCheckboxes: this.state.selectedCheckboxes})
  }

  componentWillMount() {
    var options = []
    this.setState({loading: true, selectedCoin: false})
    this.requestInstance = new Request('https://cryptohelper.io/api/icos')
    this.requestInstance.get().then((res) => {
        Object.entries(res.data).forEach(
          ([key, value]) => {
            options.push({ value: value['_id'], label: value['name']})
          }
        )
        options.sort(function(a, b) {
          return a['label'].localeCompare(b['label'])
        })
        this.setState({coins: res.data, loading: false})
      }
    );

    this.setState({options: options})

    // sad thing we have to do so IE works
    if (!Object.entries) {
      Object.entries = function( obj ) {
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
        while (i--)
          resArray[i] = [ownProps[i], obj[ownProps[i]]];
        
        return resArray;
      };
    }
  }

  didSelectCoin(value) {
    var updatedCheckboxes = new Set()

    this.requestInstance = new Request('https://cryptohelper.io/api/pull_params/' + value.value)
    this.requestInstance.get().then((res) => {
        updatedCheckboxes.add('BitcoinTalk Search')
        Object.entries(res.data).forEach(
          ([key, value]) => {
            if (!value.hasOwnProperty('placeholder')) {
              if (key == 'twitter_pull_params') {
                updatedCheckboxes.add('Twitter')
              } else if (key == 'reddit_pull_params') {
                if ('subreddit' in value) {
                  updatedCheckboxes.add('Reddit Subreddit')
                }
                if ('users' in value) {
                  updatedCheckboxes.add('Reddit User Comments')
                }
              } else if (key == 'slack_pull_params') {
                updatedCheckboxes.add('Slack')
              } else if (key == 'discord_pull_params') {
                updatedCheckboxes.add('Discord')
              } else if (key == 'telegram_pull_params') {
                updatedCheckboxes.add('Telegram')
              } else if (key == 'rocket_chat_pull_params') {
                updatedCheckboxes.add('Rocket Chat')
              } 
            }
          }
        )     
        this.setState({selectedCoin: true, 
          icoData: res.data, 
          icoName: value.value, 
          selectedCheckboxes: updatedCheckboxes})
      }
    );
  }

  render() {
    let { icoName, icoData, selectedCheckboxes } = this.state

    var feedComponents = {}
    var toggleComponents = {}

    if (icoName) {
      toggleComponents[0] = 'BitcoinTalk Search'
    }

    Object.entries(icoData).forEach(
      ([key, value]) => {
        if (!value.hasOwnProperty('placeholder')) {     
          if (key == 'twitter_pull_params') {
            toggleComponents[3] = 'Twitter'
          } else if (key == 'reddit_pull_params') {
            if ('subreddit' in value) {
              toggleComponents[2] = 'Reddit Subreddit'
            }
            if ('users' in value) {
              toggleComponents[1] = 'Reddit User Comments'
            }
          } else if (key == 'slack_pull_params') {
            toggleComponents[6] = 'Slack'
          } else if (key == 'discord_pull_params') {
            toggleComponents[5] = 'Discord'
          } else if (key == 'telegram_pull_params') {
            toggleComponents[4] = 'Telegram'
          } else if (key == 'rocket_chat_pull_params') {
            toggleComponents[7] = 'Rocket Chat'
          } 
        }
      }
    );

    if (selectedCheckboxes.has('BitcoinTalk Search')) {
      feedComponents[6] =
        <FeedContainer icon={BitcoinTalkLogo} header='BitcoinTalk Search' feedCount={selectedCheckboxes.size}>
          <Iframe src={'/analytics/bitcointalksearch#gsc.tab=0&gsc.q=' + icoName}/>
        </FeedContainer>
    }

    Object.entries(icoData).forEach(
      ([key, value]) => {
        if (!value.hasOwnProperty('placeholder')) {
          if (key == 'twitter_pull_params' && selectedCheckboxes.has('Twitter')) {
            feedComponents[3] =
              <FeedContainer icon={TwitterLogo} header='Twitter' feedCount={selectedCheckboxes.size}>
                <Embed key={icoName + 'twitter'} icoName={icoName} service='twitter'/>
              </FeedContainer>
          } else if (key == 'reddit_pull_params') {
            if (selectedCheckboxes.has('Reddit Subreddit')) {
              feedComponents[4] =
                <FeedContainer icon={RedditLogo} header='Reddit Subreddit' feedCount={selectedCheckboxes.size}>
                  <Embed key={icoName + 'reddit subreddit'} icoName={icoName} service='reddit' subservice='subreddit'/>
                </FeedContainer>
            }

            if (selectedCheckboxes.has('Reddit User Comments')) {
              feedComponents[5] =
                <FeedContainer icon={RedditLogo} header='Reddit User' feedCount={selectedCheckboxes.size}>
                  <Embed key={icoName + 'reddit user'} icoName={icoName} service='reddit' subservice='user'/>
                </FeedContainer>
            }
          } else if (key == 'slack_pull_params' && selectedCheckboxes.has('Slack')) {
            feedComponents[0] =
              <FeedContainer icon={SlackLogo} header='Slack' feedCount={selectedCheckboxes.size}>
                <Messages key={icoName + 'slack_feed'} icoName={icoName} feedType='slack_feed'/>
              </FeedContainer>
            
          } else if (key == 'discord_pull_params' && selectedCheckboxes.has('Discord')) {
            feedComponents[1] =
              <FeedContainer icon={DiscordLogo} header='Discord' feedCount={selectedCheckboxes.size}>
                <Messages key={icoName + 'discord_feed'} icoName={icoName} feedType='discord_feed'/>
              </FeedContainer>
            
          } else if (key == 'telegram_pull_params' && selectedCheckboxes.has('Telegram')) {
            feedComponents[2] =
              <FeedContainer icon={TelegramLogo} header='Telegram' feedCount={selectedCheckboxes.size}>
                <Messages key={icoName + 'telegram_feed'} icoName={icoName} feedType='telegram_feed'/>
              </FeedContainer>
          } else if (key == 'rocket_chat_pull_params' && selectedCheckboxes.has('Rocket Chat')) {
            feedComponents[2] =
              <FeedContainer icon={RocketChatLogo} header='Rocket Chat' feedCount={selectedCheckboxes.size}>
                <Messages key={icoName + 'rocket_chat_feed'} icoName={icoName} feedType='rocket_chat_feed'/>
              </FeedContainer>
          }
        }
      }
    );

    return (
      <div className='container'>
        <div className='DashboardSelectContainer'>
          <h4 
            className='Tooltip' 
            hidden={this.state.selectedCoin}>
            Select a token from the dropdown menu to pull the most important aggregated info about each token across multiple channels.
          </h4>
          <Select className='CoinSelect' 
            placeholder={this.state.loading ? 'Loading...' : 'Select a coin'}
            value={this.state.icoName}
            onChange={this.didSelectCoin.bind(this)}
            clearable={false}
            disabled={this.state.loading ? true : false}
            options={this.state.options} />
        </div>
        <div className = 'DashboardHeader'>
          <h1> { icoData['name'] } </h1>
          <div className='ToggleContainer'>
            {Object.keys(toggleComponents).map((key, index) => (
              <Checkbox
                label={toggleComponents[key]}
                handleCheckboxChange={this.toggleCheckbox.bind(this)}
                isChecked={this.state.selectedCheckboxes.has(toggleComponents[key])}
                key={index}
              />
            ))}
          </div>
        </div>
        <div className='DashboardFeeds'>
          {Object.keys(feedComponents).map((key, index) => (
            feedComponents[key]
          ))}
        </div>
      </div>
    )
  }
}

export default Dashboard;