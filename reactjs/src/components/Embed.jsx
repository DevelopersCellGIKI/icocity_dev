import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch, IndexRoute, Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import { Timeline } from 'react-twitter-widgets'
import RedditEmbed from './RedditEmbed.jsx'
import Request from 'axios-request-handler';
import './style/Embed.css';


class Embed extends Component {

  constructor(props) {
    super(props)

    this.state = {
      icoName: this.props.icoName,
      pullParams: {}
    }
  }

  componentWillMount() {
    this.requestInstance = new Request('https://cryptohelper.io/api/pull_params/' + this.props.icoName + '/' + this.props.service)
    this.requestInstance.get().then(res => {
      this.setState({'pullParams' : res.data})
    });
  }

  componentWillUnmount() {
    this.requestInstance.cancel();
  }

  render() {
    let { icoName, pullParams } = this.state

    if (Object.keys(pullParams).length === 0 && pullParams.constructor === Object) {
      return( <div> loading </div> )
    }
    
    var components = []

    if (this.props.service == 'twitter') {
      components.push(
        <div className={`TwitterContainer Scrollable`}>
          <Timeline
            className='Timeline'
            dataSource={{
              sourceType: 'profile',
              screenName: pullParams['username']
            }}
            options={{
              username: pullParams['username'],
              width: '100%'
            }}
            onLoad={() => console.log('Timeline is loaded!')}
          />
        </div>
      )
    } else if (this.props.service == 'reddit' && this.props.subservice == 'subreddit' && pullParams['subreddit']) {
      components.push(<RedditEmbed type='subreddit' subreddit={pullParams['subreddit']} />)
    } else if (this.props.service == 'reddit' && this.props.subservice == 'user' && pullParams['users'][0]) {
      components.push(<RedditEmbed type='user' user={pullParams['users'][0]} />)
    } 

    return (
      <div className="EmbedContainer">
        { components }
      </div>
    )
  }
}

export default Embed;