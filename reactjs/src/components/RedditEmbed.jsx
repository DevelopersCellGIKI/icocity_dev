import React, {Component} from 'react';
import * as firebase from 'firebase';
import {BrowserRouter as Router, Route, Switch, IndexRoute, Link} from 'react-router-dom';
import ReactDOM from 'react-dom';
import Linkify from 'react-linkify';
import './style/RedditEmbed.css';


class RedditEmbed extends Component {

  constructor(props) {
      super(props);

      this.subreddit = props.subreddit;
      this.state = {
        loading: true,
        src: ""
      };
  }
  
  componentDidMount() {
    // Create a JSONP callback that will set our state
    // with the data that comes back from the Gist site
    var redditCallback = RedditEmbed.nextRedditCallback();
    window[redditCallback] = function(gist) {
      // super shitty way to make links have target=_blank
      gist = gist.replace(/<a/g, '<a target="_blank"')
      gist = gist.replace(/href="\/r/g, 'href="https://www.reddit.com/r')
      this.setState({
        loading: false,
        src: gist
      });
    }.bind(this);

    var url;

    if (this.props.type == 'subreddit') {
      url = "https://www.reddit.com/r/" + this.props.subreddit + "/hot/.embed?limit=50&callback=" + redditCallback
    } else if (this.props.type == 'user') {
      url = "https://www.reddit.com/user/" + this.props.user + "/.embed?limit=50&sort=hot&callback=" + redditCallback
    }

    // Add the JSONP script tag to the document.
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    document.head.appendChild(script);
  }

  render() {
    if (this.state.loading) {
      return <div>loading...</div>;
    } else {
      return (
        <div className={`RedditEmbed Scrollable`}>
          <div className={ this.props.type == 'subreddit' ? 'Subreddit' : 'User' } dangerouslySetInnerHTML={{__html: this.state.src}} />
        </div>
      );
    }
  }
}

RedditEmbed.propTypes = {
    subreddit: React.PropTypes.string.isRequired, // e.g. "username/id"
};

// Each time we request a Gist, we'll need to generate a new
// global function name to serve as the JSONP callback.
var nextRedditCallbackId = 0;
RedditEmbed.nextRedditCallback = () => {
  return "embed_reddit_callback_" + nextRedditCallbackId++;
};

export default RedditEmbed;

