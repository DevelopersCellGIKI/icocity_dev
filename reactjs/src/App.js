import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import firebase from './Firebase';
import Select from 'react-select';
import Summary from './components/Summary'
// Be sure to include styles at some point, probably during your bootstrapping


class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      db: {}
    }

    firebase.database().ref().once('value').then((snapshot) => {
      this.setState({db: snapshot.val()})
    })
    
  }

  componentDidUpdate() {
    
  }

  render() {
    let { db } = this.state

    var summaryComponents = []
    Object.entries(db).forEach(
      ([key, value]) => {
        summaryComponents.push(<Summary name={key} websiteURL={value['website_url']} whitepaperURL={value['whitepaper_url']} />)
      }
    );
    return <div> {summaryComponents} </div>
  }
}

export default App;
