import React from 'react';
import { BrowserRouter } from 'react-router-dom'

import App from './App';

const Routes = (props) => (
 <Router {...props}>
   <Route path="*" component={App}>
   </Route>
 </Router>
);
export default Routes;
