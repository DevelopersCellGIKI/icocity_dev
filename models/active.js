var mongoose = require('mongoose');

// define the schema for our user model
var activeSchema = mongoose.Schema({
    name : String,
    website : String,
    name_lower : String,
    whitepaper : String,
    description : String,
    status : String,
    month : String,
    start_date : Number,
    end_date : Number

});

// create the model for users and expose it to our app
module.exports = mongoose.model('Active', activeSchema);
